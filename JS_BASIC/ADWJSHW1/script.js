"use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }

    get getName(){
        return this.name
    }
    get getAge(){
        return this.age
    }

    get getSalary(){
        return this.salary
    }

    set setName(anotherName){

        return this.name=anotherName
    }
    set setAge(anotherAge){
        return this.name=anotherAge
    }
    set setSalary(anotherSalary){
        return this.name=anotherSalary
    }
}


class Programer extends Employee{
    constructor(name, age, salary, langs) {
        super(name, age, salary);
        this.langs=langs
    }

    get getAnotherSalary() {
        return this.getSalary * 3
    }
}


const junior =new Programer("ivan",33,333,['JS', "Python", "Java"])
const midel=new Programer("vasya23",23,555,["English","Russian","Ukraine"])
console.log(junior)
console.log(midel)


// Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Это некая сушость которая равна null и являеться завершающей в цепочке наследования. Другими словами каждый объект имеет свой прототип  и может унаследовать свойства родитель и тд
