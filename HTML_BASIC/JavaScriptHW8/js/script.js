document.getElementById("price").onblur = function () {
    let textPrice = +price.value;
    price.innerText = textPrice;
    if (textPrice < 0 ) {
        document.getElementById('error').innerText = `Please enter correct price`;
        document.getElementById('price').classList.add('redOutline');
    } else {
        document.getElementById('price').classList.remove('redOutline');
        document.getElementById('error').innerText = ``;
        let spanList = document.getElementById('spanList');
        let spanFunk = document.createElement('span');
        spanFunk.setAttribute('onclick', `removespan(this)`);
        let textNode = document.createTextNode(`Текущая цена: ${textPrice}`);
        spanFunk.appendChild(textNode);
        spanList.appendChild(spanFunk);
        console.log(textPrice);
    }
};

let removespan = function(span) {
    span.parentNode.removeChild(span);
};