
// let page = document.querySelector('.page');
// let themeButton = document.querySelector('.theme-button');
// themeButton.onclick = function() {
//     page.classList.toggle('light-theme');
//     page.classList.toggle('dark-theme');
// };

let page = document.getElementsByTagName("body")[0]

let themeButton = document.querySelector('.theme-button');
 if( localStorage.getItem("bgColor")){
    let bgColor = localStorage.getItem("bgColor")
     page.classList.add(bgColor)
}


themeButton.addEventListener("click",function() {
    if (page.classList.contains("light-theme")){
        page.classList.replace('light-theme',"dark-theme");
        localStorage.setItem("bgColor","dark-theme")
    }
    else {
        page.classList.replace("dark-theme","light-theme");
        localStorage.setItem("bgColor","light-theme");
    }

});