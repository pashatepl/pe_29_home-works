let myList = document.querySelector(".myList")
function showArray(list,parentElement= document.body) {

    let resultArray = list.map(words =>  {
       return `<li>${words}</li>`
    })

        parentElement.insertAdjacentHTML("afterbegin", `<ul>${resultArray.join(' ')}</ul>>`)

}

showArray(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"],myList)